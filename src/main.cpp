#include <Arduino.h>
#include <Servo.h>

//Definindo o nome dos pinos

// Noroeste
#define NO A0
// Sudoeste
#define SO A1
// Sudeste
#define SE A2
// Norderste
#define NE A3

void moveServo(Servo servo, int quantidade);

//Base
Servo EixoX;
//Angulo do painel
Servo EixoY;
void setup()
{
  // Definindo os modos dos pinos
  pinMode(NO, INPUT);
  pinMode(SO, INPUT);
  pinMode(SE, INPUT);
  pinMode(NE, INPUT);

  //Iniciando os servos motores
  EixoX.attach(8);
  EixoY.attach(9);

  // Posicionando o painel solar em paralelo com o ceu
  EixoX.write(90);
  EixoY.write(90);
  delay(1000);
}

void loop()
{
  //Lendo os valores dos sensores
  int noroeste = analogRead(NO);
  int sudoeste = analogRead(SO);
  int sudeste = analogRead(SE);
  int nordeste = analogRead(NE);

  int leste = sudeste + nordeste;
  int sul = sudoeste + sudeste;
  int oeste = noroeste + sudoeste;
  int norte = noroeste + nordeste;

  // Quanto menor o valor maior a luminozidade

  //Verifica o eixo Y
  if (leste < oeste)
  {
    moveServo(EixoY, 1);
  }
  else if (oeste < leste)
  {
    moveServo(EixoY, -1);
  }

  // Dependendo da direção do motor Y em a movimentação do motor X e invertida
  int MoveEixoX = EixoY.read() < 90 ? -1 : 1;

  if (norte < sul)
  {
    moveServo(EixoX, MoveEixoX);
  }
  else if (sul < norte)
  {
    moveServo(EixoX, -MoveEixoX);
  }

  delay(10);
}

// Se certifica de mover o motor dentro do limite de 0 a 180 graus
void moveServo(Servo servo, int quantidade)
{
  int anguloAtual = servo.read();
  int anguloFinal = anguloAtual + quantidade;
  if (anguloFinal <= 180 && anguloFinal >= 0)
  {
    servo.write(anguloFinal);
  }
}