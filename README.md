# Introdução

Muitos dos melhores designs que vemos da atualidade são inspirados na
natureza, essa por sua vez que tem muito a nos ensinar com seus milhões de
anos de experiência, foi observando ela (e alguns vídeos no youtube) que
surgiu essa ideia, um painel solar que segue a luz do sol, idêntico a um
girassol. O ponto é que para o painel solar produzir energia ele se utiliza
de célula fotovoltaica (dispositivo que convierte a luz proveniente do Sol
diretamente em energia elétrica) e essas células perdem sua eficiência
quando os raios solares as atingem em ângulo, então o objetivo do projeto é
ter um melhor aproveitamento de energia solar. Para isso iremos utilizar
sensores LDR(resistor que varia sua resistência conforme a intensidade de
luz que incide sobre ele) para definir a posição do sol, e motores
servo(motores que possuem rotação controlada de 0 a 180 graus) para mover os
painéis na direção dele.

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

| Nome                   | gitlab user            |
| ---------------------- | ---------------------- |
| Diovani Eduardo Coelli | diovani.eduardo.coelli |
| Maria Eduarda Claro    | Maria_Eduarda_Claro    |

## Documentação

A documentação do projeto pode ser acessada pelo:

- [Gitlab pages](https://diovani.eduardo.coelli.gitlab.io/ie21cp20202)
- **O gitlab pages parece não estar funcionando então eu transferi o website para um link provisório em outro sistema de hospedagem.** [Link alternativo](https://ie21cp20202.vercel.app/)
- [Projeto no Tinkercad](https://www.tinkercad.com/things/c2TI6noWZLC)
